package com.isl.simpleapp.data

import com.isl.simpleapp.data.model.LoggedInUser
import java.io.IOException

/**
 * Class that handles authentication w/ login credentials and retrieves user information.
 */
class LoginDataSource {

    fun login(username: String, password: String): Result<LoggedInUser> {
        return try {
            if (username == "admin" && password == "admin") {
                val fakeUser = LoggedInUser(java.util.UUID.randomUUID().toString(), "Admin")
                Result.Success(fakeUser)
            } else throw IllegalAccessException()
        } catch (e: Throwable) {
            Result.Error(IOException("Error logging in", e))
        }
    }

    fun logout() {
        // TODO: revoke authentication
    }
}