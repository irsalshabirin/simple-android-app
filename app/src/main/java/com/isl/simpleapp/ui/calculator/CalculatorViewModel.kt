package com.isl.simpleapp.ui.calculator

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class CalculatorViewModel : ViewModel() {
    private var operator = ""

    internal val listOperator = listOf("+", "-", "/", "*")

    private val _text = MutableLiveData<String>().apply {
        value = "Hasil : 0"
    }
    val text: LiveData<String> = _text

    fun putOperator(value: String) {
        operator = value
    }

    fun calculate(number1: String, number2: String) {
        val n1 = number1.toLong()
        val n2 = number2.toLong()
        when (operator) {
            "+" -> _text.value = "Hasil : ${n1 + n2}"
            "-" -> _text.value = "Hasil : ${n1 - n2}"
            "/" -> {
                try {
                    _text.value = "Hasil : ${n1.div(n2)}"
                } catch (e: ArithmeticException) {
                    _text.value = "Hasil : NaN"
                }
            }
            "*" -> _text.value = "Hasil : ${n1 * n2}"
        }
    }
}