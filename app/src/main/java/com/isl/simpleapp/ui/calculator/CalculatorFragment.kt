package com.isl.simpleapp.ui.calculator

import android.R
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.isl.simpleapp.databinding.FragmentCalculatorBinding

class CalculatorFragment : Fragment() {

    private lateinit var calculatorViewModel: CalculatorViewModel
    private var _binding: FragmentCalculatorBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        calculatorViewModel = ViewModelProvider(this).get(CalculatorViewModel::class.java)

        _binding = FragmentCalculatorBinding.inflate(inflater, container, false)
        val root: View = binding.root

        binding.spinner1.also {
            it.adapter = ArrayAdapter(
                requireContext(),
                R.layout.simple_dropdown_item_1line,
                calculatorViewModel.listOperator
            )

            it.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    adapterView: AdapterView<*>,
                    view: View,
                    i: Int,
                    l: Long
                ) {
                    val value = adapterView.getItemAtPosition(i).toString()
                    calculatorViewModel.putOperator(value)
                }

                override fun onNothingSelected(adapterView: AdapterView<*>?) {}
            }
        }

        binding.acbCalculate.setOnClickListener {
            calculatorViewModel.calculate(binding.et1.text.toString(), binding.et2.text.toString())
        }

        calculatorViewModel.text.observe(viewLifecycleOwner) {
            binding.tvResult.text = it
        }
        return root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}