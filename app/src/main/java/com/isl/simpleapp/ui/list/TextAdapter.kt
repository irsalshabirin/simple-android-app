package com.isl.simpleapp.ui.list

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.isl.simpleapp.databinding.ItemTextBinding

class TextAdapter(private val list: List<String>, private val onClickListener: (Int) -> Unit) :
    RecyclerView.Adapter<TextAdapter.TextViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TextViewHolder {
        val binding = ItemTextBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return TextViewHolder(binding)
    }

    override fun getItemCount() = list.size

    override fun onBindViewHolder(holder: TextViewHolder, position: Int) {
        with(holder) {
            binding.root.setOnClickListener { onClickListener(position) }
            binding.textView.text = list[position]
        }
    }

    inner class TextViewHolder(val binding: ItemTextBinding) :
        RecyclerView.ViewHolder(binding.root)
}