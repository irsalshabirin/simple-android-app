package com.isl.simpleapp.ui.list

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class ListViewModel : ViewModel() {

    private val _data = MutableLiveData<List<String>>().apply {
        value = (0..99).map { "List ke-${it + 1}" }
    }
    val data: LiveData<List<String>> = _data
}